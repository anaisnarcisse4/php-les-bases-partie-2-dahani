<?php

// On fixe une condition : le nombre de paramètre à minimum 2 paramètres
// pour pouvoir exécuter le script

if ($argc > 2) {
    // on définit une variable équivalente au deuxième élément du argv:

    $mot = $argv[1];

    // on crée un nouveau tableau en retirant le premier argument du tableau:
    $arr = array_splice($argv, 2);

    // on crée un nouveau tableau  :

    $tab = [];

    // $tab2 = array_splice($arr, 1);

    foreach ($arr as $value) {
        $value2 = preg_split('/\W|\s+/', $value, -1, PREG_SPLIT_NO_EMPTY);

        @$tab[$value2[0]] = $value2[1];
    }

    if (count($value2, 0) == 2) {
        echo $tab[$mot] . "\n";
    }
}

// $k[0] = $tab1[0];
// $tab2[1] = $tab1[1];
// $a = array_fill_keys($keys, $tab);
// var_dump($tab);
