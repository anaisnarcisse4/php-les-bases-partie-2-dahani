<?php

// Setting a cookie
if ($_GET['action'] == 'set') {
    setcookie($_GET['name'], $_GET['value'], time() + 3600);
}
// lire le cookie:
elseif (isset($_COOKIE[$_GET['name']]) && ($_GET['action'] == 'get')) {
    echo $_COOKIE[$_GET['name']] . "\n";
}

// delete cookie
elseif ($_GET['action'] == 'del') {
    // setcookie($_GET['name'], $_GET['value'], time()-3600);
    setcookie($_GET['name'], '');
}
